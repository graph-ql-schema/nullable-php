<?php

namespace GraphQlNullableField\Tests;

use GraphQL\Error\Error;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use GraphQlNullableField\Nullable;
use PHPUnit\Framework\TestCase;

class NullableTest extends TestCase
{
    /**
     * Набор данных для тестирования инициализации скаляра nullable для различных типов
     *
     * @return array
     */
    public function dataForNullableTypeInit() {
        return [
            [Type::int(), "NullableInt"],
            [Type::id(), "NullableID"],
            [Type::string(), "NullableString"],
        ];
    }

    /**
     * Тестирование создания скалярного типа Nullable
     *
     * @dataProvider dataForNullableTypeInit
     * @param ScalarType $baseType
     * @param string $typeName
     */
    public function testNullableTypeInit(ScalarType $baseType, string $typeName) {
        $type = Nullable::create($baseType);

        $this->assertEquals($typeName, $type->name);
    }

    /**
     * Набор данных для тестирования конвертации значения для скалярного Nullable типа
     *
     * @return array
     */
    public function dataForNullableTypeValueConvertation() {
        return [
            [Type::int(), 0, 0],
            [Type::int(), "null", null],
            [Type::id(), "null", null],
            [Type::id(), "24", "24"],
            [Type::string(), "undefined", null],
            [Type::string(), "test", "test"],
        ];
    }

    /**
     * Тестирование конвертации значения для скалярного Nullable типа
     *
     * @dataProvider dataForNullableTypeValueConvertation
     * @param ScalarType $baseType
     * @param string $baseValue
     * @param $result
     * @throws Error
     */
    public function testNullableTypeValueConvertation(ScalarType $baseType, $baseValue, $result) {
        $type = Nullable::create($baseType);
        $value = $type->parseValue($baseValue);

        $this->assertEquals($result, $value);
    }

    /**
     * Набор данных для проверки метода isNullable
     *
     * @return array
     */
    public function dataForIsNullable() {
        return [
            [Nullable::create(Type::int()), true],
            [Nullable::create(Type::string()), true],
            [Type::id(), false],
            [Nullable::create(Type::id()), true],
        ];
    }

    /**
     * Тестирование метода isNullable на переданном наборе данных. Метод должен корректно определять Nullable скаляры
     *
     * @dataProvider dataForIsNullable
     * @param ScalarType $type
     * @param bool $result
     */
    public function testIsNullable(ScalarType $type, bool $result) {
        $this->assertEquals($result, Nullable::isNullable($type));
    }

    /**
     * Набо данных для тестирования метода getBaseType.
     */
    public function dataForGetBaseType() {
        return [
            [Type::int(), Type::int()],
            [Nullable::create(Type::int()), Type::int()],
            [Nullable::create(Type::id()), Type::id()],
        ];
    }

    /**
     * Тестирование метода getBaseType. Должен корректно возвращаться базовый тип для переданного скаляра
     *
     * @dataProvider dataForGetBaseType
     * @param ScalarType $nullableType
     * @param ScalarType $result
     */
    public function testGetBaseType(ScalarType $nullableType, ScalarType $result) {
        $this->assertEquals($result, Nullable::getBaseType($nullableType));
    }
}