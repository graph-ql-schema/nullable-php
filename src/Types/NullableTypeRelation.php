<?php

namespace GraphQlNullableField\Types;

use GraphQL\Type\Definition\ScalarType;

/**
 * Элемент коллекции для сохранения связи между базовым скалярным типом GraphQL и конечным Nullable типом
 */
class NullableTypeRelation
{
    /**
     * Базовый скалярный тип
     *
     * @var ScalarType
     */
    private $baseType;

    /**
     * Nullable скалярный тип
     *
     * @var ScalarType
     */
    private $nullableType;
    
    /**
     * NullableTypeRelation constructor.
     *
     * @param ScalarType $baseType
     * @param ScalarType $nullableType
     */
    public function __construct(ScalarType $baseType, ScalarType $nullableType)
    {
        $this->baseType = $baseType;
        $this->nullableType = $nullableType;
    }
    
    /**
     * Геттер базового типа
     *
     * @return ScalarType
     */
    public function getBaseType(): ScalarType
    {
        return $this->baseType;
    }
    
    /**
     * Геттер скалярного типа
     *
     * @return ScalarType
     */
    public function getNullableType(): ScalarType
    {
        return $this->nullableType;
    }
}