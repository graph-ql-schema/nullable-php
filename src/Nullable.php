<?php

namespace GraphQlNullableField;

use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\CustomScalarType;
use GraphQL\Type\Definition\ScalarType;
use GraphQlNullableField\Store\NullableTypesCollection;
use GraphQlNullableField\Types\NullableTypeRelation;

/**
 * Фасад библиотеки. Предназначен исключительно для вызова статических методов
 */
class Nullable
{
    /**
     * Nullable constructor.
     *
     * Данный конструктор не доступен для конечного пользователя
     */
    private function __construct()
    {
    }

    /**
     * Create Nullable scalar
     *
     * Basic scalar types cannot handle Null values for scalars, even if they are declared
     * without "!". In fact, for an argument, a value of the form Int! and Int is the same type, which will not allow
     * passing null as a server-side value. This type solves this problem and allows you to transfer as a value for it Null
     * or Undefined, which in fact will be setting Null value
     *
     * @param ScalarType $baseScalarType
     * @return ScalarType
     */
    public static function create(ScalarType $baseScalarType): ScalarType
    {
        $nullableTypesCollection = NullableTypesCollection::getInstance();

        // Если тип создавался ранее, то нет смысла инициализировать его заново
        if ($nullableTypesCollection->offsetExists($baseScalarType->name)) {
            /** @var NullableTypeRelation $type */
            $type = $nullableTypesCollection[$baseScalarType->name];

            return $type->getNullableType();
        }

        // Инициализируем новый скалярный тип на основе переданного
        $name = sprintf('Nullable%s', $baseScalarType->name);
        $customScalarType = new CustomScalarType(
            [
                'name'         => $name,
                'description'  => sprintf(
                    "The scalar type `%s` is a combination of` NULL` values and values of type `%s`. Values of the basic type can be passed in the format in which this type accepts them. For example, for a number, you can pass a value without quotes. To pass `NULL` values, you must pass them in string format, namely `\"null\"`, `\"undefined\"`. This limitation is due to the fact that the system prohibits the transfer of values not reserved by ENUM types.",
                    $name,
                    $baseScalarType->name
                ),
                'serialize'    => function ($value) use ($baseScalarType) {
                    if ("null" === $value || "undefined" === $value) {
                        return null;
                    }

                    return $baseScalarType->serialize($value);
                },
                'parseValue'   => function ($value) use ($baseScalarType) {
                    if ("null" === $value || "undefined" === $value) {
                        return null;
                    }

                    return $baseScalarType->parseValue($value);
                },
                'parseLiteral' => function (Node $valueNode, ?array $variables = null) use ($baseScalarType) {
                    if ($valueNode instanceof StringValueNode) {
                        $literalValue = strtolower($valueNode->value);
                        if (in_array($literalValue, ["null", "undefined"])) {
                            return null;
                        }
                    }

                    return $baseScalarType->parseLiteral($valueNode, $variables);
                },
            ]
        );

        $nullableTypesCollection[$baseScalarType->name] = new NullableTypeRelation($baseScalarType, $customScalarType);
        return $customScalarType;
    }

    /**
     * Checks if a value is Nullable
     *
     * @param ScalarType $scalar
     * @return bool
     */
    public static function isNullable(ScalarType $scalar): bool
    {
        $nullableTypesCollection = NullableTypesCollection::getInstance();

        foreach ($nullableTypesCollection as $key => $nullableType) {
            /** @var NullableTypeRelation $nullableType */
            if ($nullableType->getNullableType()->name == $scalar->name) {
                return true;
            }
        }

        return false;
    }

    /**
     * Getting the base type for nullable scalar
     *
     * @param ScalarType $scalar
     * @return ScalarType
     */
    public static function getBaseType(ScalarType $scalar): ScalarType
    {
        $nullableTypesCollection = NullableTypesCollection::getInstance();

        foreach ($nullableTypesCollection as $key => $nullableType) {
            /** @var NullableTypeRelation $nullableType */
            if ($nullableType->getNullableType()->name == $scalar->name) {
                return $nullableType->getBaseType();
            }
        }

        return $scalar;
    }
}